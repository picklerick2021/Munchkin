import os
from typing import Union, Callable
import matplotlib.pyplot
import nbformat as nbf
from options import Options
from find_pairs import FindPairs
import plotting as plot
from plotting import Player
import copy

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from mpl_toolkits.mplot3d.art3d import Line3DCollection
from mpl_toolkits.mplot3d.axes3d import Axes3D
import mpl_toolkits

def create_report(fp: FindPairs, op: Options):
    """
    Creates a report in Jupyter notebook format.
    Args:
        fp: The FindPairs object holding population, pairs and history.
        op: options dataclass.
    """
    # from run import Options
    filename: str = 'test.ipynb'
    if op.report == '':
        if op.seed is None:
            seed = np.random.get_state()[1][0]
            op.seed = seed
        else:
            seed = op.seed
        filename = f'./reports/{op.finder}_seed_{seed}_dims_{op.dims}_size_{op.size}.ipynb'
    else:
        filename = op.report

    nb = nbf.v4.new_notebook()
    header = f"""# Report for {op.finder} running with {op.size} pairs in {op.dims} dimensions."""


    opr = copy.deepcopy(op)
    opr.apns = None
    opr.report = None
    code = f"""# About PairFinderFramework: https://gitlab.com/picklerick2021/Munchkin/-/tree/main
# Read the docs: https://picklerick2021.gitlab.io/Munchkin/run_main/
#%matplotlib notebook
%matplotlib widget
import sys
sys.path.append('..')
from options import Options
from run import *
import report
import matplotlib.pyplot as plt

plt.rcParams["figure.figsize"] = 6, 6
op = {opr}

fp = main(op)
"""

    head_Pairs = f"""# Pairs"""
    code_Pairs = f"""for pair in fp.pairs:
    print(f'{{pair[0]}} -> {{pair[1]}} from {{fp.population[pair[0]]}} to {{fp.population[pair[1]]}} = {{fp.distance_function_1(fp.population[pair[0]], fp.population[pair[1]])}}')
"""

    # https://stackoverflow.com/questions/43445103/inline-animations-in-jupyter
    head_PairAni = f"""# Pair Animation"""
    code_PairAni = f"""# for hist in fp.history:
    # fig = plot.plot_population(fp.population, hist[1])
fig, ax, lc, ani = report.show_pair_animation(fp, op)
"""

    nb['cells'] = [nbf.v4.new_markdown_cell(header),
                   nbf.v4.new_code_cell(code),
                   nbf.v4.new_markdown_cell(head_PairAni),
                   nbf.v4.new_code_cell(code_PairAni),
                   nbf.v4.new_markdown_cell(head_Pairs),
                   nbf.v4.new_code_cell(code_Pairs),
                   ]

    dir_of_filename = os.path.dirname(filename)
    if dir_of_filename != '':
        os.makedirs(os.path.dirname(filename), exist_ok=True)
    nbf.write(nb, filename)
    print(f'Saved report to {filename}.')


def show_pair_animation(fp: FindPairs, op: Options) \
        -> (matplotlib.pyplot.Figure, Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D],
            Union[LineCollection, Line3DCollection], Player):
    """
    Creates Figure with animation of Finder results.
    Args:
        fp: The FindPairs object holding population, pairs and history.
        op: options dataclass.

    Returns:
        - Matplotlib Figure object.
        - Matplotlib Axes or Axes3D object.
        - Matplotlib LineCollection or LineCollection3D
        - Animation object derived from FuncAnimation. Keep this object valid to keep animation running.
    """
    fig, ax, lc, ani = plot.plot_pair_animation(fp.population, fp.history)
    return fig, ax, lc, ani
