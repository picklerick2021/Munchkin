from dataclasses import dataclass
import dataclasses as dc
import argparse


@dataclass
class Options:
    dims: int = 2
    size: int = 10
    seed: any = None
    max_runtime: int = 29900
    finder: str = None
    custom: str = None
    report: str = None
    apns: argparse.Namespace = None

    def parse_args(self, apns: argparse.Namespace):
        self.apns = apns
        field: dc.Field
        for field in dc.fields(self):
            if field.name in apns:
                self.__setattr__(field.name, getattr(apns, field.name))
