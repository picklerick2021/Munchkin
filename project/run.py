from find_pairs import *
import argparse
from options import Options

import sys
import os

sys.path.append("./finders")


def import_custom(module: str):
    """
    Import custom module.
    Args:
        module: python module
    """
    if module.lower().endswith('.py'):
        module = module[:-3]
    if os.path.isfile(module + '.py'):
        path = os.path.dirname(module + '.py')
        if path == '':
            path = '.'
        sys.path.append(path)
    try:
        import importlib
        importlib.import_module(module)
    except ModuleNotFoundError:
        print(f'Custom finder {module} not found!')
        exit()


def main(options: Options) -> FindPairs:
    """
    Main function.
    Args:
        options: options dataclass.

    Returns:
        Finder results.
    """
    if options.seed is None:
        options.seed = np.random.randint(0, np.iinfo(np.int32).max)
    if options.custom is not None:
        import_custom(options.custom)
    np.random.seed(options.seed)
    print(f'Using seed {options.seed}')

    manager = FindPairManager(options)
    if options.report is not None:
        manager.create_report()
    else:
        manager.run_finder()
        manager.check_result()
    return manager.results[options.finder].fp


class NewlineFormatter(argparse.HelpFormatter):
    def _split_lines(self, text, width):
        if '\n' in text:
            return text.splitlines()
            # this is the RawTextHelpFormatter._split_lines
        return argparse.HelpFormatter._split_lines(self, text, width)


def create_parser(options: Options, *args, **kwargs) -> argparse.ArgumentParser:
    if 'formatter_class' not in kwargs:
        kwargs['formatter_class'] = NewlineFormatter
    new_parser = argparse.ArgumentParser(add_help=False, *args, **kwargs)
    new_parser.add_argument('-h', '--help', action='store_true', default=False,
                            help='show this help message and exit')
    new_parser.add_argument('--dims', type=int, default=options.dims, help='number of dimensions')
    new_parser.add_argument('--size', type=int, default=options.size, help='number of points (even)')
    new_parser.add_argument('--seed', type=int, default=options.seed, help='initial seed')
    new_parser.add_argument('--max_runtime', type=int, default=options.max_runtime, help='maximum time passed to finder')
    new_parser.add_argument('--finder', type=str, default=options.finder,
                            help='available finders: \n' + ',\n'.join(pair_finders), metavar='')
    new_parser.add_argument('--custom', type=str, default=None, help='custom finder file, e.g., finder.py')
    new_parser.add_argument('--report', type=str, nargs='?', default=None, const='',
                            help='create report either with default filename or specified filename')
    return new_parser


def init_commandline() -> (argparse.ArgumentParser, Options):
    """
    Initialize options from commandline arguments.
    Returns:
        Initialized options.
    """
    options = Options(finder=pair_finders[0])
    parser = create_parser(options)
    args = parser.parse_args()
    # if args.custom is not None:
    #    args.finder = args.custom
    options.parse_args(args)
    return parser, options


def init_name_is_main():
    """
    Called if `__name__ == __main__`.
    """
    parser, options = init_commandline()
    if options.custom is not None:
        import_custom(options.custom)
        parser, options = init_commandline()
    if options.apns.help:
        print(parser.format_help())
    else:
        main(options)


if __name__ == "__main__":
    init_name_is_main()
