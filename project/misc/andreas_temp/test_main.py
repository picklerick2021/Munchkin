from main import *
# import pytest


def test_single():
    points: list[Single] = [Single(-10), Single(-1), Single(-2), Single(1)]
    pairs: dict[int, Pair] = get_double_pairs(points)
    # print_pairs(points, pairs)
    assert pairs[0].members == [0, 2] and pairs[1].members == [1, 3]
