#!/bin/env python

import math
import random
import time


class Object(object):
    def __init__(self, coord: list[float], prec: int = 9):
        self.coord: list[float] = coord
        self.prec = prec

    def __str__(self):
        # return str(self.coord)
        return f'({", ".join(format(x, ".3f") for x in self.coord)})'

    @property
    def x(self) -> float:
        try:
            return self.coord[0]
        except IndexError:
            return None

    @property
    def y(self) -> float:
        try:
            return self.coord[1]
        except IndexError:
            return None

    @property
    def z(self) -> float:
        try:
            return self.coord[2]
        except IndexError:
            return None

    def distance_to_object(self, other: 'Object') -> float:
        return self.distance_to_coord(other.coord)

    def distance_to_coord(self, coord) -> float:
        if len(self.coord) == len(coord):
            return round(math.sqrt(sum([math.pow(coord[c] - self.coord[c], 2)
                                        for c in range(len(self.coord))])), self.prec)
        else:
            return None


class Single(Object):
    def __init__(self, x: float):
        super(Single, self).__init__([x])


class Point(Object):
    def __init__(self, x: float, y: float):
        super(Point, self).__init__([x, y])
        # self.coord: list[float] = coord


class Pair(object):
    def __init__(self, me: int, dist: float, smallest: bool = True):
        self.members: list[int] = [me]
        self.dist = dist
        self.smallest = smallest

    def append(self, other: int):
        self.members.append(other)

    def reset(self, other: int, dist: float):
        self.members = [self.members[0], other]
        self.dist = dist

    def check(self, other: int, dist: float):
        if (self.smallest and dist < self.dist) or (not self.smallest and dist > self.dist):
            self.reset(other, dist)
        elif dist == self.dist:
            self.append(other)


def get_double_pairs(points: list[Object], rand: bool = False, dist_init: float = 1e99) -> dict[int, Pair]:
    pairs: dict[int, Pair] = {}
    out: list[int] = []

    cpoint: int
    point: Point

    cpoint, pair = new_get_pair(points, out, rand, dist_init)
    while len(points) - len(out) > 1:
        repair = get_pair(pair.members[1], points, out, dist_init)
        if sorted(pair.members) == sorted(repair.members):
            pairs[cpoint] = pair
            out.extend(pairs[cpoint].members)
            if len(points) - len(out) > 1:
                cpoint, pair = new_get_pair(points, out, rand, dist_init)
        else:
            cpoint = pair.members[1]
            pair = repair
    return pairs


def new_get_pair(points: list[Object], out: list[int], rand: bool = False, dist_init: float = 1e99) -> (int, Pair):
    oopen = [i for i in range(len(points)) if i not in out]
    # cpoint = random.randrange(len(points))
    if rand:
        cpoint = random.choice(oopen)
    else:
        maxpos = 0
        cpoint = oopen[0]
        zero = Single(0)
        # if isinstance(points[0], Single):
        #     zero = Single(0)
        # el
        if isinstance(points[0], Point):
            zero = Point(0, 0)
        for i in oopen:
            dis = points[i].distance_to_object(zero)
            if dis > maxpos:
                cpoint = i
                maxpos = dis
    pair = get_pair(cpoint, points, out, dist_init)
    return cpoint, pair


def get_pair(cpoint: int, points: list[Object], out: list[int], dist_init: float = 1e99) -> Pair:
    pair: Pair = Pair(cpoint, dist_init)

    cother: int
    for cother in range(len(points)):
        # for cother in range(len(points)):
        if cother not in out and cother != cpoint:
            # if cother != cpoint:
            pair.check(cother, points[cpoint].distance_to_object(points[cother]))
    return pair


def print_pairs(points: list[Object], pairs: dict[int, Pair], verbose: bool = False):
    dsum = 0.
    for pair in pairs.values():
        if verbose:
            spair = ", ".join([f'{cpoint+1}: {points[cpoint]}' for cpoint in pair.members])
            print('d = {dist:.3f} for {pairs}'.format(dist=pair.dist, pairs=spair))
        dsum += pair.dist * (len(pair.members) - 1)
        # print(str(points[0]))
    print(f'Sum is {dsum:.3f}.')


def timit(func):
    a = time.time_ns() / 1e9
    func()
    b = time.time_ns() / 1e9
    print(f"Time: {b-a:.1f} s.")


def main():
    random.seed(1)
    points: list[Point] = [Point(random.random(), random.random()) for c in range(1000)]
    # others: list[Point] = copy.deepcopy(points)
    pairs = get_double_pairs(points, True)
    print_pairs(points, pairs, True)

    pairs = get_double_pairs(points)
    print_pairs(points, pairs, True)

    print("With random:")
    print_pairs(points, pairs, False)



if __name__ == "__main__":
    timit(main)
    # test()
    # main()
