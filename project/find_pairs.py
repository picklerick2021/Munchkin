from population import Population
import plotting as plot
import math, time
import numpy as np
# import sys
# import os
from abc import ABC, ABCMeta, abstractmethod
from options import Options
# import report
from dataclasses import dataclass


pair_finders = []
pair_finders_funcs = {}


def register_pair_finder(finder_func=None, default=False):
    """
    Decorator to register a finder to the framework. Must implement run and return if finder is reliable.\n
    **Example:** \n
    ```python
    from find_pairs import Finder, register_pair_finder

    @register_pair_finder(default=True)
    class MyFinderClass(Finder):
        '''
        Needs a docstring
        Args:
            *args (tuple): Pass args to finder.
            **kwargs (dict): Pass key value args to finder.

        Returns:
            bool: Reliable finder?
        '''
        def run(self, *args, **kwargs) -> bool:
            return True
    ```
    """
    def decorator(finder):
        if default and len(pair_finders) > 0:
            pair_finders.insert(0, finder.__name__)
        else:
            pair_finders.append(finder.__name__)
        pair_finders_funcs[finder.__name__] = finder
        return finder

    if finder_func:
        return decorator(finder_func)
    return decorator


class FindPairs(Population):
    def __init__(self, dims, size, lower_limit=-10, upper_limit=10, rand=True, population=None):
        super().__init__(dims, size, lower_limit, upper_limit, rand, population)
        # self.pairs = np.empty((int(size / 2), 2), dtype=np.int8)
        self.pairs = np.arange(size, dtype=np.int8).reshape(int(size / 2), 2)
        self.history = np.empty((0, 2), dtype=object)
        self.add_to_history()
        self.ani = []

    def add_to_history(self):
        """
        Add current state (pair distance sum, list of pairs) to history.
        """
        self.history = np.vstack([self.history, np.array([self.pair_distance_sum(), self.pairs.copy()], dtype=object)])

    def pair_distance_sum(self) -> float:
        """
        Calculate pair distance sum of current pairing.
        Returns:
            Sum of pair distances.
        """
        distance_sum = sum([self.distance_function_1(
            self.population[self.pairs[i, 0]], self.population[self.pairs[i, 1]])
            for i in range(np.shape(self.pairs)[0])]
        )
        return distance_sum

    def run_finder(self, finder: str, max_runtime=29900) -> bool:
        """
        Run the specified finder.
        Args:
            finder: name of the finder.
            max_runtime: maximum runtime in milliseconds.

        Returns:
            Preliminary flag of finder (true == final; false == preliminary)
        """
        print(f'Calling {finder}...')
        if finder in pair_finders and finder in pair_finders_funcs.keys():
            func = pair_finders_funcs[finder]
            if isinstance(func, ABCMeta) and issubclass(func, Finder):
                fp: Finder
                fp = func(self.dims, self.size, self.lower_limit, self.upper_limit, self.rand, self.population)
                ret = fp.run(max_runtime=max_runtime)
                self.population = fp.population
                self.pairs = fp.pairs
                self.history = fp.history
                return ret
            elif type(func).__name__ == 'function':
                func = getattr(self, finder)
                return func(max_runtime=max_runtime)
        else:
            return False

    def check_pairs(self) -> bool:
        """
        Checks if the pairs are valid.
        Returns:
            Validity of pairs.
        """
        valid = True
        if np.any(np.unique(self.pairs) != np.arange(self.size)):
            valid = False
        if np.shape(self.pairs) != (int(self.size / 2), 2):
            valid = False
        return valid

    def plot_pairs(self, info=None, windowtitle=None):
        """
        Plot pairing diagram.
        Args:
            info: Suptitle of window.
            windowtitle: Window caption.
        """
        fig, ax, lc = plot.plot_population(self.population, self.pairs, info=info, windowtitle=windowtitle)

    def plot_history_dsums(self, info=None, windowtitle=None):
        """
        Plot history of distance sums.
        Args:
            info: Suptitle of window.
            windowtitle: Window caption.
        """
        fig, ax, artist = plot.plot_history_dsums(self.history[:, 0], info=info, windowtitle=windowtitle)

    def plot_pairs_history(self, info=None, windowtitle=None):
        """
        Plot history of pairs as animation.
        Args:
            info: Suptitle of window.
            windowtitle: Window caption.
        """
        fig, ax, lc, ani = plot.plot_pairs_history(self.population, self.history, self.history[:, 0], info=info, windowtitle=windowtitle)
        self.ani.append(ani)

    def plot_show(self):
        """
        Display all plots.
        """
        plot.plot_show()


class Finder(FindPairs, metaclass=ABCMeta):
    def __init__(self, dims = None, size = None, lower_limit=-10, upper_limit=10, rand=True, population=None):
        if dims is not None and size is not None:
            super().__init__(dims, size, lower_limit, upper_limit, rand, population)

    def __call__(self, *args, **kwargs) -> bool:
        return self.run(*args, **kwargs)

    @abstractmethod
    def run(self, *args, **kwargs) -> bool:
        pass


@dataclass
class ResultSet:
    ret: bool = False
    fp: Finder = None


class FindPairManager():
    def __init__(self, options: Options):
        self.options = options
        self.results: dict[str, ResultSet] = {}

    def create_finder(self, finder=None) -> ResultSet:
        """
        Creates a result set with finder.
        Args:
            finder: name of the finder.
        Returns:
            Empty result set with initialized finder.
        """
        result = ResultSet()

        if finder is None:
            finder = self.options.finder
        if finder in pair_finders and finder in pair_finders_funcs.keys():
            func = pair_finders_funcs[finder]
            if isinstance(func, ABCMeta) and issubclass(func, Finder):
                result.fp = func(self.options.dims, self.options.size)
            elif type(func).__name__ == 'function':
                result.fp = FindPairs(self.options.dims, self.options.size)

        self.results[finder] = result
        return result

    def run_finder(self, finder=None) -> bool:
        """
        Run the specified finder.
        Args:
            finder: name of the finder.

        Returns:
            Preliminary flag of finder (true == final; false == preliminary)
        """
        result = ResultSet()

        if finder is None:
            finder = self.options.finder
        if finder in pair_finders and finder in pair_finders_funcs.keys():
            if finder not in self.results:
                result = self.create_finder(finder)
            else:
                result = self.results[finder]
            print(f'Calling {finder}...')
            if isinstance(result.fp, Finder):
                result.ret = result.fp.run(max_runtime=self.options.max_runtime)
            elif isinstance(result.fp, FindPairs):
                result.ret = result.fp.run_finder(finder=finder, max_runtime=self.options.max_runtime)
            elif type(result.fp).__name__ == 'function':
                result.ret = result.fp

        return result.ret

    def create_report(self, finder=None):
        """
        Creates a report for finder.
        Args:
            finder: Specify finder or None for finder in options.
        """
        import report
        if finder is None:
            finder = self.options.finder
        if finder in pair_finders and finder in pair_finders_funcs.keys():
            if finder not in self.results:
                result = self.create_finder(finder)
            else:
                result = self.results[finder]
            report.create_report(result.fp, self.options)

    def check_result(self, finder=None) -> bool:
        """
        Checks if the pairs are valid.
        Args:
            finder: Specify finder or None for finder in options.
        Returns:
            Validity of pairs.
        """
        if finder is None:
            finder = self.options.finder
        if finder in self.results:
            result: ResultSet = self.results[finder]
            fp: Finder = result.fp
            if not result.ret:
                print('The finder results are considered to be preliminary.')
            if fp.check_pairs():
                dist_sum = fp.pair_distance_sum()
                print('The pairs are valid!\nSum of all pair distances {}'.format(dist_sum))
                fp.plot_pairs_history(info=f'Sum of all pair distances {dist_sum:.3f}', windowtitle=finder)
                fp.plot_pairs(info=f'Sum of all pair distances {dist_sum:.3f}', windowtitle=finder)
                fp.plot_history_dsums(windowtitle=finder)
                fp.plot_show()
            else:
                print('The pairs are invalid')
            return fp

            return self.results[finder].check_pairs()
        return False


@register_pair_finder()
class Dual(Finder):
    """
    Very fast start values based on:
        on particle a look for closest distance to particle b and check if a is also the closest particle for b
        -> then accepts the pair -> imperfect by design -> fails test -> return False!
    Args:
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, *args, **kwargs) -> bool:
        from finders import find_pairs_dual_pairs as fpdp
        last = self.history[-1]
        fpdp.dual_pairs_main(self)
        if last[0] < self.history[-1][0]:
            self.pairs = last[1]
            self.add_to_history()
        return False  # return True if functional


@register_pair_finder()
class Permutation(Finder):
    """
    Random permutation of pairs for a given time.
    Args:
        tzero: Start time calculated by time.time_ns() / 1e6
        max_runtime: The finder will stop running after given milli seconds.
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        Reliable finder?
    """
    def run(self, tzero=None, max_runtime=29900, *args, **kwargs) -> bool:
        cycles = min(1000000, math.factorial(self.size) * 10)  # TODO math.factorial(100) -> too large
        dsum = self.pair_distance_sum()
        # best = len(self.history) - 1
        if tzero is None:
            tzero = time.time_ns() / 1e6

        cycle = 0
        while cycle < cycles and (time.time_ns() / 1e6 - tzero) <= max_runtime:  # 30000 ms
            cycle += 1

            # TODO: test for only two particles
            ipair1 = np.random.choice(int(self.size / 2))  # random choice of particles
            ipair2 = ipair1
            while ipair2 == ipair1:
                ipair2 = np.random.choice(int(self.size / 2))

            pair1 = np.copy(self.pairs[ipair1])
            pair2 = np.copy(self.pairs[ipair2])
            perm1 = np.random.choice([0, 1])
            perm2 = np.random.choice([0, 1])

            # permutation of pairs
            self.pairs[ipair1][perm1], self.pairs[ipair2][perm2] = self.pairs[ipair2][perm2], self.pairs[ipair1][perm1]

            # TODO: implement epsilon random chance decay
            dsnew = self.pair_distance_sum()
            if dsnew >= dsum:
                # chance = np.random.random()
                # if chance < 1.1:
                self.pairs[ipair1] = pair1
                self.pairs[ipair2] = pair2
            # else:
            #     dsum = dsnew
            #     self.add_to_history()
            else:
                dsum = dsnew
                self.add_to_history()
        print(f"Time: {(time.time_ns() / 1e6 - tzero):.1f} ms.")
        return True  # return True if functional


@register_pair_finder()
class Dual_permutation(Dual, Permutation):
    """
    Combination of dual finder and then permutation finder.
    Args:
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, tzero=None, max_runtime=29900, *args, **kwargs) -> bool:
        tzero = time.time_ns() / 1e6
        Dual.run(self, *args, **kwargs)
        Permutation.run(self, tzero=tzero, max_runtime=max_runtime, *args, **kwargs)
        return True  # return True if functional


@register_pair_finder()
class Cluster(Finder):
    """
    Cluster finder.
    Args:
        max_runtime: The finder will stop running after given milli seconds.
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, max_runtime=29900, *args, **kwargs) -> bool:
        from misc_funs import labels_to_could_be_pairs, could_be_pairs_to_id
        from finders.find_pairs_cluster import Cluster as ClusterFinder

        cluster = ClusterFinder(self)

        labels, pop = cluster.run()

        qpairs = labels_to_could_be_pairs(labels, self.population)
        pairs = could_be_pairs_to_id(qpairs, self.population)

        self.population = pop
        self.pairs = pairs

        return False  # return True if functional


@register_pair_finder()
class Neural_network(Finder):
    """
    Neural network finder.
    Args:
        max_runtime: The finder will stop running after given milli seconds.
        obj (list): Pass internal objects.
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, max_runtime=29900, obj = [], *args, **kwargs) -> bool:
        from finders.find_pairs_neural_network import NeuralNetwork

        neuralnetwork = NeuralNetwork(self)
        neuralnetwork.run()
        obj.append(neuralnetwork)

        return False  # return True if functional


@register_pair_finder()
class Nearest_neighbour(Finder):
    """
    Nearest neighbour finder.
    Args:
        max_runtime: The finder will stop running after given milli seconds.
        obj (list): Pass internal objects.
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, max_runtime=29900, obj = [], *args, **kwargs) -> bool:
        from finders.find_pairs_nearest_neighbour import NearestNeighbour

        nearestneighbour = NearestNeighbour(self)
        nearestneighbour.run()
        obj.append(nearestneighbour)

        return False  # return True if functional


@register_pair_finder()
class Center_of_gravity(Finder):
    """
    Determines the center of gravity of all particles. The particles farest away from the center of gravity is determined
    then it is connected with its nearest neighbour. This step is repeated iterative until all particles are connected.

    Args:
        tzero: Start time calculated by time.time_ns() / 1e6
        max_runtime: The finder will stop running after given milli seconds.
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, tzero=None, max_runtime=29900, *args, **kwargs) -> bool:
        from finders.find_pairs_center_of_gravity import center_of_gravity
        center_of_gravity(self)
        return False  # return True if functional


@register_pair_finder()
class Center_permutation(Center_of_gravity, Permutation):
    """
    Center of gravity finder followed by permutation finder.

    Args:
        max_runtime: The finder will stop running after given milli seconds.
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, max_runtime=29900, *args, **kwargs) -> bool:
        tzero = time.time_ns() / 1e6
        # from finders.find_pairs_center_of_gravity import center_of_gravity
        # center_of_gravity(self)
        # self.permutation(tzero=tzero, max_runtime=max_runtime)
        Center_of_gravity.run(self, *args, **kwargs)
        Permutation.run(self, tzero=tzero, max_runtime=max_runtime, *args, **kwargs)
        return True  # return True if functional


@register_pair_finder(default=True)
class Baseline_pair_finding_method(Dual_permutation):
    """
    Baseline finder.

    Args:
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, tzero=None, max_runtime=29900, *args, **kwargs) -> bool:
        tzero = time.time_ns() / 1e6
        Dual_permutation.run(self, tzero=tzero, max_runtime=max_runtime, *args, **kwargs)
        return True  # return True if functional
