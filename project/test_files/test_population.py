import sys
sys.path.append('../finders')

import pytest
from population import Population
import numpy as np


@pytest.mark.parametrize('p1_, p2_, dis',
                         [
                             ([12], [9], np.sqrt((12 - 9) ** 2)),
                             ([4, 2], [1, 0], np.sqrt((4 - 1) ** 2 + (2 - 0) ** 2)),
                             ([4, 3, 4], [2, 2, 3], np.sqrt((4 - 2) ** 2 + (3 - 2) ** 2 + (4 - 3) ** 2)),
                             ([1, 2, 3, 4], [4, 2, 3, 1],
                              np.sqrt((1 - 4) ** 2 + (2 - 2) ** 2 + (3 - 3) ** 2 + (4 - 1) ** 2))
                         ])
def test_distance_function_1(p1_, p2_, dis):
    p1 = np.array(p1_)
    p2 = np.array(p2_)

    a = Population.distance_function_1(p1, p2)

    assert a == dis


def test_size_pop():
    pop = Population(2, 10)
    assert np.mod(pop.size, 2) == 0


@pytest.mark.parametrize('dims',
                         [
                             1,
                             2,
                             3
                         ])
@pytest.mark.parametrize('size',
                         [
                             5,
                             10,
                             100
                         ])
def test_distance_function_1(dims, size):
    p1 = Population(dims, size)
    assert p1.dims == dims and p1.size == size + size % 2


@pytest.mark.parametrize('low',
                         [
                             -10,
                             -100,
                         ])
@pytest.mark.parametrize('high',
                         [
                             10,
                             100
                         ])
def test_population_limits(low, high):
    p1 = Population(1, 10, low, high)
    assert p1.population.min() >= low and p1.population.max() <= high


@pytest.mark.parametrize("test_points",
                         [
                             ([-10, -1, -2, 1]),
                             ([[-10, 1], [-1, 2], [-2, 3], [1, 4]]),
                             ([[-10, 1, -1], [-1, 2, -2], [-2, 3, -3], [1, 4, 0]])
                         ])
def test_population_as_input(test_points: list[int]):
    if np.ndim(test_points) == 1:
        test_points = np.array(test_points).reshape(len(test_points), 1)
    pop: Population = Population(np.shape(test_points)[1], len(test_points), population=test_points)
    assert np.all(pop.population == np.reshape(test_points, (len(test_points), np.shape(test_points)[1])))
