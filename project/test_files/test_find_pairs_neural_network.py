import sys
sys.path.append('../finders')

from find_pairs import FindPairs, Neural_network
import numpy as np
import warnings


def test_NeuralNetwork():
    pass


def test_History():
    from finders.find_pairs_neural_network import NeuralNetwork

    fp = FindPairs(2, 10)

    h = NeuralNetwork.History()

    rnd_range = 10

    h.actor_output = [k for k in range(rnd_range)]
    h.critic_output = [k for k in range(rnd_range)]
    h.reward = [k for k in range(rnd_range)]

    length = [len(k) for k in h][0]

    b = []

    for aval, cval, reward in h:
        assert aval in h.actor_output
        assert cval in h.critic_output
        assert reward in h.reward

        b.append(aval + cval + reward)

    assert b == list(np.arange(length * rnd_range)[0::length])

    h.reset()

    assert [k for k in h] == []


def test_neural_network():
    dim = 2
    size = 10

    fp = Neural_network(dim, size)

    old_loss = fp.pair_distance_sum()
    obj = []
    fp.run(obj=obj)

    NN = obj[0]

    best_loss = NN.best_loss
    best_learned_loss = NN.best_learned_loss

    assert [k for k in NN.history] != []

    fpds = fp.pair_distance_sum()
    if not fpds < old_loss:
        warnings.warn(UserWarning(f"Result ({fpds}) should be smaller than inital distance sum ({old_loss})!"))
    assert NN.dims == dim
    assert NN.size == size

    assert best_learned_loss <= best_loss


def test_getaval():
    from finders.find_pairs_neural_network import NeuralNetwork

    fp = FindPairs(2, 10)

    aval = np.squeeze(NeuralNetwork.get_actor_output(NeuralNetwork(fp), np.expand_dims(np.transpose(fp.population), 0)))

    assert np.all((aval >= 0) & (aval <= 1))


def test_getActor():
    from finders.find_pairs_neural_network import NeuralNetwork
    dim = 2
    size = 10

    fp = FindPairs(dim, size)
    from tensorflow.keras import Model

    model: Model = NeuralNetwork.get_actor((dim, size))

    assert model.output[0].shape == size
    assert model.input[0].shape == (dim, size)


def test_getCritic():
    from finders.find_pairs_neural_network import NeuralNetwork
    dim = 2
    size = 10

    fp = FindPairs(dim, size)
    from tensorflow.keras import Model

    model: Model = NeuralNetwork.get_critic((dim, size), size)

    assert model.output[0].shape == 1
    assert model.input[0][0].shape == (dim, size)
    assert model.input[1][0].shape == size


def test_PostProcessOutput():
    from finders.find_pairs_neural_network import NeuralNetwork

    dim = 2
    size = 10

    fp = FindPairs(dim, size)
    out = np.random.random(fp.size)

    labels = NeuralNetwork.post_process_actor_output(out, fp.size)

    assert max(labels) < size / 2
    assert np.all([isinstance(k, np.int32) or isinstance(k, np.int64) for k in labels])
    assert np.all(np.unique(labels, return_counts=True)[1] == 2)