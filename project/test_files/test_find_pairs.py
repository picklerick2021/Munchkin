import sys
sys.path.append('../finders')

import pytest
from find_pairs import FindPairs, pair_finders
from population import Population
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

from itertools import permutations

import types


def test_find_pairs_shape():
    size: int = 10
    fp = FindPairs(2, size)
    assert fp.pairs.shape == (size / 2, 2)


@pytest.mark.parametrize("dim",
                         [
                             1
                         ])
@pytest.mark.parametrize("size",
                         [
                             10,
                             20,
                             50,
                             100
                         ])
def test_find_pairs_distance_sum(dim, size):
    fp = FindPairs(dim, size)
    a = np.linspace(0, size - 1, size, dtype=int)
    x1 = a[0::2]
    x2 = a[1::2]
    x = np.transpose([x1, x2])
    fp.pairs = x
    fp.run_finder('Dual')
    assert fp.pair_distance_sum() > 0


# @pytest.mark.parametrize("test_points",
#                          [
#                              ([-10, -1, -2, 1])
#                          ])
# def test_my_pair_finding_method_A(test_points: list[int]):
#     pop = Population(np.ndim(test_points), len(test_points))
#     pop.population = test_points
#     fp = FindPairs(np.ndim(test_points), len(test_points), population=pop)
#     result = fp.my_pair_finding_method()
#     assert result == [(0, 2), (1, 3)]  # (1, 3), (2, 4)


@pytest.mark.parametrize("test_points",
                         [
                             ([-10, -1, -2, 1]),
                         ])
def test_finders(test_points: list[int]):
    """validation for the online example"""

    pop: Population = Population(np.ndim(test_points), len(test_points), population=test_points)
    # pop.population = np.array(test_points)
    for finder in pair_finders:
        print('')
        try:
            fp = FindPairs(np.ndim(test_points), len(test_points), population=pop)
            result = fp.run_finder(finder)
        except:
            return False
        if result:
            assert np.all(fp.pairs == [(0, 2), (1, 3)]) or \
                   np.all(fp.pairs == [(0, 2), (3, 1)]) or \
                   np.all(fp.pairs == [(2, 0), (1, 3)]) or \
                   np.all(fp.pairs == [(2, 0), (3, 1)]) or \
                   np.all(fp.pairs == [(1, 3), (0, 2)]) or \
                   np.all(fp.pairs == [(1, 3), (2, 0)]) or \
                   np.all(fp.pairs == [(3, 1), (2, 0)]) or \
                   np.all(fp.pairs == [(3, 1), (0, 2)])
            assert fp.check_pairs()
            assert fp.pair_distance_sum() == 10.0
        else:
            print(f'Finder {finder} is not yet operational!')


def create_grid(size, dim, lower=-10, upper=10):
    size = size + size % 2
    lu = upper - lower
    ret = np.zeros((size, dim))
    pop = Population(dim, size, lower, upper)
    pairs = np.arange(size, dtype=np.int8).reshape(int(size / 2), 2)
    dsum = 0
    for i in range(0, len(ret), 2):
        for j in range(len(ret[i])):
            ret[i][j] = lower + lu / size * i
            ret[i + 1][j] = lower + lu / size * (i + 0.25)
        dsum += pop.distance_function_1(ret[i], ret[i + 1])
    # np.random.shuffle(ret)
    # p = np.random.permutation(len(ret))
    # return ret[p], pairs.flatten()[p].reshape(int(size / 2), 2), dsum
    pp = np.random.permutation(len(pairs))
    retp = ret.reshape(int(size / 2), 2 * dim)[pp].reshape(size, dim)
    return retp, pairs[pp], dsum


@pytest.mark.parametrize("grid",
                         [
                             (create_grid(4, 1)),
                             (create_grid(100, 2)),
                         ])
def test_finders_grid(grid):
    """validation for grid example and clear result"""

    test_points, pairs, dsum = grid
    pop: Population = Population(np.shape(test_points)[1], len(test_points), population=test_points)
    # pop.population = np.array(test_points)
    for finder in pair_finders:
        print('')
        try:
            fp = FindPairs(np.ndim(test_points), len(test_points), population=pop)
            result = fp.run_finder(finder, max_runtime=1000)
        except:
            result = False
        if result:
            assert np.all(np.sort(fp.pairs, 0) == np.sort(pairs, 0))
            assert fp.check_pairs()
            assert np.round(fp.pair_distance_sum(), 5) == np.round(dsum, 5)
        else:
            print(f'Finder {finder} is not yet operational!')


def test_check_pair():
    size: int = 10
    fp = FindPairs(2, size)
    fp.pairs = np.empty((int(size + 2 / 2), 2), dtype=np.int8)
    assert fp.check_pairs() is False

    fp = FindPairs(2, size)
    fp.pairs[:, 0] = 0
    assert fp.check_pairs() is False

    fp = FindPairs(2, size)
    fp.pairs[:, 0] = np.arange(size / 2)
    fp.pairs[:, 1] = np.arange(size / 2, size)
    assert fp.check_pairs() is True


def test_center_of_gravity_pairfinder():
    fp = FindPairs(2, 10)
    fp.run_finder('Center')
    assert fp.check_pairs()
    pass