import numpy as np


class Population:
    def __init__(self, dims: int, size: int, lower_limit: float = -10, upper_limit: float = 10, rand:bool = True,
                 population: 'Population' = None):
        """
        Population object.
        Args:
            dims: Dimension.
            size: Number of particles (must be equal).
            lower_limit: Lower coordinate limit (same for each dimension).
            upper_limit: Upper coordinate limit (same for each dimension).
            rand: if True, the population is initialized randomly,
                    if False, population must be massed as argument
            population: Inital population.
        """

        if size % 2 == 1:
            print('Size of the population must be even. \nChanging size from {} to {}'.format(size, size + 1))
            size += 1
        if population is None:
            print('Initializing the population of size {} in {} dimensions in range[{},{}]'.format(
                size, dims, lower_limit, upper_limit))
            self.population = np.random.uniform(low=lower_limit, high=upper_limit, size=(size, dims))
        elif isinstance(population, Population):
            self.population = population.population
        else:
            self.population = np.reshape(population, (size, dims))

        self.size = size
        self.dims = dims
        self.lower_limit = lower_limit
        self.upper_limit = upper_limit
        self.rand = rand

    @staticmethod
    def distance_function_1(p1: np.ndarray, p2: np.ndarray) -> float:
        """
        Calculates Euclidean distance.
        Args:
            p1: Particle one.
            p2: Particle two.

        Returns:
            Distance as float
        """
        return np.sqrt(np.sum((p1 - p2) ** 2))
