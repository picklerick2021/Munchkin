import sys
import os
sys.path.append(os.path.dirname(__file__))
print(sys.path)

from dataclasses import Field

from options import Options
import run
from find_pairs import *

import argparse

def define_env(env):
    "Hook function"

    @env.macro
    def list_pair_finders():
        return '<ul><li>{}</li></ul>'.format('</li><li>'.join(pair_finders))

    @env.macro
    def print_argparse_help():
        def formatter(prog: str):
            return run.NewlineFormatter('', width=60)

        options = Options(finder=pair_finders[0])
        parser = run.create_parser(options, formatter_class=formatter)

        ret: str = f'<pre style="white-space: pre-wrap;">{parser.format_help().replace("usage: mkdocs", "usage:")}</pre>'
        return ret

    @env.macro
    def print_options_dataclass() :
        def get_rows_and_column_keys(options: Options) -> (list[str], list[str]):
            rows: list[str] = list(options.__dataclass_fields__.keys())
            cols: list[str] = options.__dataclass_fields__[rows[0]].__slots__
            return rows, cols

        def get_table_row_from_field(row_field: Field, returned_columns: list[str], cols: list[str]) -> str:
            ret: str = '<tr>'
            for col in returned_columns:
                column_key = col.lower()
                if column_key in cols:
                    if column_key == 'type':
                        ret += get_object_type_table_cell(row_field, column_key)
                    elif column_key == 'default':
                        ret += get_default_value_table_cell(row_field, column_key)
                    else:
                        ret += get_string_value_table_cell(row_field, column_key)
            ret += '</tr>'
            return ret

        def get_object_name(object) -> str:
            try:
                module: str = ['', object.__module__ + '.'][object.__module__ != 'builtins']
                name: str = '{}{}'.format(module, object.__name__)
            except AttributeError:
                name: str = str(object)
            return name

        def get_object_type_table_cell(row_field, column_key) -> str:
            object = getattr(row_field, column_key)
            name: str = get_object_name(object)
            return '<td><code>{}</code></td>'.format(name)

        def get_default_value_table_cell(row_field, column_key) -> str:
            object = getattr(row_field, column_key)
            if object.__class__.__name__ == '_MISSING_TYPE':
                return '<td><em>{}</em></td>'.format('required')
            else:
                return '<td><code>{}</code></td>'.format(object)

        def get_string_value_table_cell(row_field, column_key) -> str:
            return '<td><code>{}</code></td>'.format(str(getattr(row_field, column_key)))

        returned_columns: list[str] = ['Name', 'Type', 'Default']
        options: Options = Options(finder=pair_finders[0])
        rows, cols = get_rows_and_column_keys(options)

        ret = '<table><thead><tr><th>'
        ret += '</th><th>'.join(returned_columns) + '</th></tr></thead><tbody>'

        for row in rows:
            row_field: Field = options.__dataclass_fields__[row]
            ret += get_table_row_from_field(row_field, returned_columns, cols)
        ret += '</tbody></table>'

        return ret