import sys #it is needed in getattr(sys, 'gettrace', None)

from find_pairs import FindPairs
import numpy as np
from dataclasses import dataclass
import time
import tensorflow as tf
from tensorflow.python.framework.ops import EagerTensor
from tensorflow.keras import Model

class NeuralNetwork:
    """
    Class using a neural network to find the best pairs with minimal summed distance
    """

    def __init__(self, fp: 'FindPairs'):
        import tensorflow.keras as keras
        self.find_pairs = fp
        self.dims = self.find_pairs.dims
        self.size = self.find_pairs.size

        self.actor = self.get_actor((self.dims, self.size))
        self.critic = self.get_critic((self.dims, self.size), self.size)
        self.actor_optimiser = keras.optimizers.Adadelta(learning_rate=1e-1)
        self.critic_optimiser = keras.optimizers.Adam(learning_rate=1e-3)

        self.history = self.History()

        self.best_loss = None
        self.best_learned_loss = None

        pass

    def plotstuff(self, hist):  # TODO
        import matplotlib.pyplot as plt
        plt.plot(np.squeeze(hist))
        plt.show()

    def run(self) -> None:
        """
        Executes the neural network pair finder
        Looping for 30 seconds trying to develop a good actor and critic model for predicting good labels
        Returns:
            nothing
        """
        from misc_funs import labels_to_could_be_pairs, could_be_pairs_to_id

        tf.random.set_seed(0)  # TODO: ignores the global seed
        np.random.seed(0)

        is_alive = True

        start_time = time.time()

        population = self.find_pairs.population
        state = np.expand_dims(np.transpose(population), 0)

        reward = 0
        learned_reward = 0

        best_weights = self.actor.get_weights()

        while is_alive:
            self.history.reset()

            for k in range(1):

                actor_output = self.get_actor_output(state)
                critic_output = self.critic([state, actor_output])

                self.history.critic_output.append(critic_output)
                random_action = np.random.random(np.shape(actor_output))

                if k == 0:
                    self.history.actor_output.append(actor_output)
                    actor_output = np.squeeze(actor_output)
                    labels = self.post_process_actor_output(actor_output, self.size)
                else:
                    self.history.actor_output.append(random_action)
                    random_action = np.squeeze(random_action)

                    labels = self.post_process_actor_output(random_action, self.size)

                could_be_pairs = labels_to_could_be_pairs(labels, population)
                could_be_pairs_id = could_be_pairs_to_id(could_be_pairs, population)

                self.find_pairs.pairs = could_be_pairs_id

                loss = self.find_pairs.pair_distance_sum()
                reward = -loss / 100

                if k == 0:
                    learned_reward = reward

                self.history.reward.append(reward)
                self.history.labels.append(labels)
                self.history.loss.append(loss)

                if self.best_loss is None or self.best_loss > loss:
                    self.best_loss = loss
                    best_weights = self.actor.get_weights()

                if k == 0 and (self.best_learned_loss is None or self.best_learned_loss > loss):
                    self.best_learned_loss = loss

            critic_loss = []

            with tf.GradientTape() as tape:
                for actor_output, critic_output, reward in self.history:
                    critic_output = self.critic([state, actor_output])
                    critic_loss.append(tf.math.square(critic_output - reward))
                    pass

                critic_loss = tf.math.reduce_mean(critic_loss)

                critic_grads = tape.gradient(tf.convert_to_tensor(critic_loss), self.critic.trainable_variables)
                self.critic_optimiser.apply_gradients((zip(critic_grads, self.critic.trainable_variables)))

            with tf.GradientTape() as tape:
                actor_output = self.get_actor_output(state)

                if abs(float(self.critic([state, actor_output])) - learned_reward) < 0.01:
                    actor_loss = -self.critic([state, actor_output])
                    actor_grads = tape.gradient(tf.convert_to_tensor(actor_loss), self.actor.trainable_variables)
                    self.actor_optimiser.apply_gradients((zip(actor_grads, self.actor.trainable_variables)))
                    self.history.actor_loss.append(actor_loss)

            self.history.critic_loss.append(critic_loss)

            end_time = time.time()

            if (end_time - start_time) > 30 and not getattr(sys, 'gettrace', None):
                # if (ed - st):
                is_alive = False

            if len(self.history.actor_loss) > 50 \
                    and np.sum(np.abs(np.array(self.history.actor_loss[-10:])
                                      - self.history.actor_loss[-1])) < 0.01:
                is_alive = False

            pass

        self.actor.set_weights(best_weights)

        actor_output = self.get_actor_output(np.expand_dims(np.transpose(self.find_pairs.population), 0))
        actor_output = np.squeeze(actor_output)

        labels = self.post_process_actor_output(actor_output, self.size)
        could_be_pairs = labels_to_could_be_pairs(labels, self.find_pairs.population)
        could_be_pairs_id = could_be_pairs_to_id(could_be_pairs, self.find_pairs.population)

        self.pairs = could_be_pairs_id
        loss = self.find_pairs.pair_distance_sum()
        minimum_loss_in_history = np.min(self.history.loss)
        pass

    def get_actor_output(self, state: EagerTensor) -> EagerTensor:
        """
        Get the action values
        Args:
            state: input to the neural network

        Returns:
            action values
        """

        aval = self.actor(state)

        aval = (aval - tf.reduce_min(aval)) / (tf.reduce_max(aval) - tf.reduce_min(aval))

        return aval

    @staticmethod
    def get_actor(shape: tuple) -> Model:
        """
        Initialises the neural network for the actor
        Args:
            shape: size of dimensions of the input

        Returns:
            keras model
        """
        from tensorflow.keras.layers import Dense, Input, Flatten

        input = Input(shape=shape)
        flatten1 = Flatten()(input)
        dense1 = Dense(64, activation="relu")(flatten1)
        dense2 = Dense(64, activation="relu")(dense1)
        dense3 = Dense(64, activation="relu")(dense2)
        dense4 = Dense(64, activation="relu")(dense3)
        output = Dense(shape[1])(dense4)

        return Model(inputs=input, outputs=output)

    @staticmethod
    def get_critic(state_shape: tuple, action_shape: tuple) -> Model:
        """
        Initialises the neural network for the critic
        Args:
            state_shape: size of the dimension for the state input\n
            action_shape: siz eof the dimensions for the action input

        Returns:
            keras model
        """
        from tensorflow.keras.layers import Dense, Input, Flatten, Concatenate
        from tensorflow.keras import Model

        state_input = Input(shape=state_shape)
        state_flatten = Flatten()(state_input)
        state_dense1 = Dense(64, activation="relu")(state_flatten)

        action_input = Input(shape=action_shape)
        action_dense1 = Dense(64, activation="relu")(action_input)

        concat = Concatenate()([state_dense1, action_dense1])

        out_dense1 = Dense(64, activation="relu")(concat)
        out_dense2 = Dense(64, activation="relu")(out_dense1)
        out_dense3 = Dense(64, activation="relu")(out_dense2)
        output = Dense(1)(out_dense3)

        return Model(inputs=[state_input, action_input], outputs=output)

    @staticmethod
    def post_process_actor_output(out: EagerTensor, size: float):
        """
        Converting the output of the actor to labels readable by the framework
        Args:
            out: action values
            size: length of the population array

        Returns:
            labels readable by the framework
        """
        if max(np.unique(out, return_counts=True)[1]) > 1:
            raise ValueError("Network output has duplicate values. Try different seed.")

        labels = np.zeros_like(out)

        k = 0

        out_temp = out.copy()

        while len(labels[labels == 0]) != 2 and k * 2 < size:
            c = np.where(np.delete(out_temp, 0)[(np.abs(np.delete(out_temp, 0) - out_temp[0])).argmin()] == out_temp)[0][0]
            labels[np.where(out_temp[0] == out)[0][0]] = k
            labels[np.where(out_temp[c] == out)[0][0]] = k

            out_temp = np.delete(out_temp, [0, c])

            k += 1
            pass

        return labels.astype(int)

    @dataclass
    class History:
        """
        Dataclass for keeping track of different history arrays
        """
        loss = []
        reward = []
        labels = []
        actor_output = []
        critic_output = []

        critic_loss = []
        actor_loss = []

        def reset(self) -> None:
            """
            Function for resetting the (relevant) history arrays each episode
            """
            self.reward.clear()
            self.labels.clear()
            self.actor_output.clear()
            self.critic_output.clear()

        def __iter__(self) -> zip:
            """
            Function for providing the ability to iterate over the dataclass
            Returns:
                zipped history
            """
            return zip(self.actor_output, self.critic_output, self.reward)
