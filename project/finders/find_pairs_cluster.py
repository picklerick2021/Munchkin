from find_pairs import FindPairs
from sklearn.cluster import *
from sklearn.neighbors import *

from misc_funs import *


class Cluster:
    """
    Class using a sci-kit cluster method to find the best pairs with minimal summed distance
    """

    def __init__(self, find_pairs: 'FindPairs'):
        self.fp = find_pairs

    def run(self) -> tuple[np.ndarray, np.ndarray]:
        """
        Main function running the pair finding method
        Returns:
            labels and modified population
        """
        population = self.fp.population
        population_transposed = np.transpose(population)

        nearestneighbour_solution = self.calc_nearestneighbour(population)

        pairs_done = [np.any([np.all(k) for k in nearestneighbour_solution == np.flip(m)]) for m in
                      nearestneighbour_solution]
        pairs_not_done = np.invert(pairs_done)

        kmeans = self.fit_kmeans_with_population(population[pairs_not_done])

        labels = self.recalc_centroids(kmeans, population[pairs_not_done])

        population_solved = np.zeros(len(nearestneighbour_solution[pairs_done]), dtype=int)

        population_solved_temp = nearestneighbour_solution[pairs_done]

        label_counter = int(max(labels) + 1)

        while len(population_solved_temp) > 0:
            k = population_solved_temp[0, :]
            population_solved[np.where(k == nearestneighbour_solution[pairs_done])[0][0]] = label_counter
            population_solved[np.where(np.flip(k) == nearestneighbour_solution[pairs_done])[0][0]] = label_counter
            population_solved_temp = np.delete(population_solved_temp, [np.where(k == population_solved_temp)[0][0],
                                                                        np.where(np.flip(k) == population_solved_temp)[
                                                                            0][0]], axis=0)
            label_counter += 1

        population = np.vstack((population[pairs_not_done], population[pairs_done]))

        labels = np.hstack((labels, population_solved))
        return labels, population

    @staticmethod
    def calc_nearestneighbour(population: np.ndarray) -> np.ndarray:
        """
        Calculates the nearestneighbours as a starting point
        Args:
            population: population of the FindPairs class

        Returns:
            indices of the nearest neighbour for each particle

        """
        nc = NearestNeighbors(n_neighbors=2, algorithm='ball_tree')
        nc.fit(population)

        distances, indices = nc.kneighbors(population)

        return indices

    @staticmethod
    def calc_centroid(arr: np.ndarray) -> np.ndarray:
        """
        Calculates the centroid for X amount of particles in Y dimensions
        Args:
            arr: array of particles

        Returns:
            Centroid in Y dimensions
        """
        length = arr.shape[0]
        sum_x = np.sum(arr[:, 0])
        sum_y = np.sum(arr[:, 1])
        return np.array([sum_x / length, sum_y / length])

    @staticmethod
    def recalc_centroids(kmeans: KMeans, population: np.ndarray) -> np.ndarray:
        """
        If a centroid has more than two members it will shift the member furthest away to its next closest centroid and
        recalculates the centroids of the new clusters
        Args:
            kmeans: KMeans object
            population: population of the FindPairs class

        Returns:
            new labels
        """

        def calc_centroid(arr: np.ndarray) -> list:
            """
            Calculates a centroid from an input array
            Args:
                arr: array of coordinates

            Returns:
                coordinates of the centroid
            """
            length = arr.shape[0]

            return [np.sum(k) / length for k in arr.transpose()]

        centroids = kmeans.cluster_centers_
        labels = kmeans.labels_

        number_of_jumps = np.zeros(len(population))

        while not np.all(np.unique(labels, return_counts=True)[1] == 2):

            for k in range(int(len(population) / 2)):
                centroid = centroids[k]
                population_for_label_k = population[labels == k]

                if len(population_for_label_k) > 2:
                    distances_to_centroid = np.array([np.linalg.norm(centroid - m) for m in population_for_label_k])
                    local_index_of_particle_with_max_distance = \
                    np.where(distances_to_centroid == np.max(distances_to_centroid))[0][0]
                    population_index_of_particle_with_max_distance = \
                    np.where(population[labels == k][local_index_of_particle_with_max_distance] == population)[0][0]

                    worst_particle_distance_to_other_centroids = np.array(
                        [np.linalg.norm(population_for_label_k[local_index_of_particle_with_max_distance] - m) for m in
                         centroids])

                    next_best_fit_for_particle = np.where(worst_particle_distance_to_other_centroids ==
                                    np.sort(worst_particle_distance_to_other_centroids)[
                                        int(number_of_jumps[population_index_of_particle_with_max_distance]) + 1])[0][0]

                    labels[population_index_of_particle_with_max_distance] = next_best_fit_for_particle
                    number_of_jumps[population_index_of_particle_with_max_distance] += 1

                pass

            new_centroids = np.array([calc_centroid(population[labels == k]) for k in range(int(len(population) / 2))])

            # ca = KMeans(n_clusters=int(len(pop) / 2), init=newCentroids)
            centroids = new_centroids

            could_be_pairs = [[] for k in range(max(labels) + 1)]

            for i, k in enumerate(labels):
                could_be_pairs[k].append(population[i])

            for k in could_be_pairs:
                if k:
                    k = np.transpose(k)
                    # plt.plot(k[0], k[1])

            # plt.show()
            pass

        return labels

    @staticmethod
    def fit_kmeans_with_population(population: np.ndarray) -> KMeans:
        """
        Initialises a KMeans object and fits the population
        Args:
            population: population of the FindPairs class

        Returns:
            returns a fitted KMeans object
        """
        kmeans = KMeans(n_clusters=int(len(population) / 2))
        kmeans.fit(population)
        return kmeans
