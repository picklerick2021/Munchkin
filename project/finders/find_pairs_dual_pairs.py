import numpy as np


class Pair(object):
    def __init__(self, me: int, dist: float, smallest: bool = True):
        self.members: list[int] = [me]
        self.dist = dist
        self.smallest = smallest

    def append(self, other: int):
        self.members.append(other)

    def reset(self, other: int, dist: float):
        self.members = [self.members[0], other]
        self.dist = dist

    def check(self, other: int, dist: float):
        if (self.smallest and dist < self.dist) or (not self.smallest and dist > self.dist):
            self.reset(other, dist)
        elif dist == self.dist:
            self.append(other)


def new_get_pair(points: np.array, out: list[int], rand: bool = False,
                 dist_init: float = float('inf')) -> (int, Pair):
    oopen = [i for i in range(len(points)) if i not in out]
    # cpoint = random.randrange(len(points))
    if rand:
        cpoint = np.random.choice(oopen)
    else:
        maxpos = 0
        cpoint = oopen[0]
        zero = np.zeros_like(points[0])
        dim = points.shape[1]
        for i in range(dim):
            zero[i] = np.max(points[:, i])
        for i in oopen:
            # dis = points[i].distance_to_object(zero)
            dis = np.linalg.norm(points[i] - zero)
            if dis > maxpos:
                cpoint = i
                maxpos = dis
    pair = get_pair(cpoint, points, out, dist_init)
    return cpoint, pair


def get_pair(cpoint: int, points: np.array, out: list[int], dist_init: float = float('inf')) -> Pair:
    pair: Pair = Pair(cpoint, dist_init)

    cother: int
    for cother in range(len(points)):
        # for cother in range(len(points)):
        if cother not in out and cother != cpoint:
            # if cother != cpoint:
            # pair.check(cother, points[cpoint].distance_to_object(points[cother]))
            dis = np.linalg.norm(points[cpoint] - points[cother])
            pair.check(cother, dis)
    return pair


def dual_pairs_main(self):
    pairs: dict[int, Pair] = {}
    out: list[int] = []

    cpoint: int
    rand = False
    dist_init: float = float('inf')

    cpoint, pair = new_get_pair(self.population, out, rand, dist_init)
    while len(self.population) - len(out) > 1:
        repair = get_pair(pair.members[1], self.population, out, dist_init)
        if sorted(pair.members) == sorted(repair.members):
            pairs[cpoint] = pair
            out.extend(pairs[cpoint].members)
            self.pairs[len(pairs) - 1] = pairs[cpoint].members
            if len(self.population) - len(out) > 1:
                cpoint, pair = new_get_pair(self.population, out, rand, dist_init)
        else:
            cpoint = pair.members[1]
            pair = repair
    # plist = list(pairs.values())
    # for i in range(len(plist)):
    #     self.pairs[i] = np.array(plist[i].members)
    self.add_to_history()
    return pairs
