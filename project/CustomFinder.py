from find_pairs import Finder, register_pair_finder


@register_pair_finder(default=True)
class Custom(Finder):
    """
    Very fast start values based on:
        on particle a look for closest distance to particle b and check if a is also the closest particle for b
        -> then accepts the pair -> imperfect by design -> fails test -> return False!
    Args:
        *args (tuple): Pass args to finder.
        **kwargs (dict): Pass key value args to finder.

    Returns:
        bool: Reliable finder?
    """
    def run(self, *args, **kwargs) -> bool:
        print(__file__)
        from finders import find_pairs_dual_pairs as fpdp
        last = self.history[-1]
        fpdp.dual_pairs_main(self)
        if last[0] < self.history[-1][0]:
            self.pairs = last[1]
            self.add_to_history()
        return False  # return True if functional
