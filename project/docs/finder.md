# Finder documentation

## Registered finders
You can add your own finders using a custom python file.
See [register_pair_finder](#find_pairs.register_pair_finder) and [commandline](../run_main/#commandline-arguments) documentation.
{{ list_pair_finders() }}

::: find_pairs