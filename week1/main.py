import itertools
import math
import time
import argparse
import numpy as np


def ap_sum(n, first, last):
    return n / 2 * (first + last)


def main(divisors, upper):
    # computes sum of all the multiples of "divisors" less than "upper".
    # note: divisors chould not me factors or multiples of each other

    # sum of AP of multiples of each divisor individually
    print(divisors, upper)

    start = time.time()
    sum1_list = [
        ap_sum(int(upper / divisors[i]), divisors[i], upper - (upper % divisors[i])) if upper % divisors[i] != 0
        else ap_sum(int(upper / divisors[i]) - 1, divisors[i], upper - divisors[i])
        for i in range(len(divisors))]

    # sum of AP of multiples of
    _divisors_combined = list(itertools.chain(
        *[[math.prod(i) for i in list(itertools.combinations(divisors, j))] for j in range(2, len(divisors) + 1)]))
    sum2_list = [ap_sum(int(upper / _divisors_combined[i]), _divisors_combined[i],
                        upper - (upper % _divisors_combined[i])) if upper % _divisors_combined[i] != 0
                 else ap_sum(int(upper / _divisors_combined[i]) - 1, _divisors_combined[i],
                             upper - _divisors_combined[i])
                 for i in range(len(_divisors_combined))]

    sol1 = int(sum(sum1_list) - sum(sum2_list))
    stop = time.time()
    print('Solution 1 : {} | Time: {}'.format(sol1, stop - start))

    start = time.time()
    sol2 = sum(x for x in range(1000) if x % 3 == 0 or x % 5 == 0)
    stop = time.time()
    print('Solution 2 : {} | Time: {}'.format(sol2, stop - start))

    start = time.time()
    sol3 = \
        [sum(k[(np.mod(k, 5) == 0) | (np.mod(k, 3) == 0)]) for k in
         [np.linspace(1, 1000 - 1, 1000 - 1, dtype=int)]][
            0]
    stop = time.time()
    print('Solution 3 : {} | Time: {}'.format(sol3, stop - start))

    start = time.time()
    sum: int = 0
    for i in range(1000):
        if i % 3 == 0 or i % 5 == 0 and i != 0:
            sum += i

    sol4 = sum
    stop = time.time()
    print('Solution 4 : {} | Time: {}'.format(sol4, stop - start))



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--divisors', nargs='+', type=int)
    parser.add_argument('-u', '--upper', type=int)
    args = parser.parse_args()
    main(args.divisors, args.upper)
